# Flappy Bird game implementation in 8x8 arduino Matrix

## Materials Used
* Arduino Uno R3
* 19 Jump wires
* 1 tact switch
* 16 50-Ohm resistors

## Concept
* An 8x8 LED matrix has 16 pins. One can only control the LED's per row and column. If one wishes to control each LED individually, it must be done so by a series of quick on and offs of the corresponding LED's.
* Since the switching is too fast, one cannot see the process of turning it off and turning it back on. And thus, it will be displayed as if you are controlling each individual LED's.
* Hence, it is similar to the concept of speed mirage as seen on "The Flash" where one moves so fast as if that one became two.

## Procedure
* Connect pins 1-16 fo the LED matrix to the corresponding 1-16 pins of the Arduino.
* Use the Analog Pins as digital pins, effectively, A0 - 14, A1- 15, A2-16.
* Connect pin 1 of the LED to A-16.
* Setup the tactile switch, connecting it to the 5v and its ground with a 50ohm resistor then the opposite pin to pin A4.
* After that, run the source code on the arduino, it should now be playable.

## Libraries Used
* This project made use of the Timer Library.

## Gameplay
* Upon pressing the reset button of the Arduino, the game will start.
* Press the tact switch for the dot to move up.
* Avoid hitting the obstacles.
* Score is computed for every obstacle that is passed through.
* Upon game over, your score will be displayed.
