#include <Event.h>
#include <Timer.h>

#include <SoftwareSerial.h>



int score = 0;
int assigned = 0;
const byte cols[] = {13, 3, 4, 10, 6, 11, 15, 16};
const byte rows[] = {9, 14, 8, 12, 17, 7, 2, 5};
int x = 0;
int y = 0;
const int obs_speed = 500;
const int gravity = 350;
const int inputPin = 18;
int buttonState;
int lastButtonState = LOW;

long lastDebounceTime = 0;
long debounceDelay = 50;
int fallID = 0;
int move2ID = 0;
int move1ID = 0;


int pixels[8][8];

Timer t;

class PixelDot
{
public:
    int row;
    PixelDot()
    {
        row = 5;
    }
    void fall()
    {
        destroy();
        if(row!=7)
            row++;
        create();
    }
    void jump()
    {
        destroy();
        if(row!=0)
            row--;
        create();
    }
    void create()
    {
        pixels[row][1] = LOW;
    }
    void destroy()
    {
        pixels[row][1] = HIGH;
    }
};

PixelDot bird;
class Obstacle
{
private:
public:
    int start;
    int col;
    Obstacle()
    {
        randomize();
        col = 7;
    }
    void create()
    {
        for(int i = 0; i < 8; i++)
            pixels[i][col] = LOW;
        for(int i = start; i < start+3; i++)
            {
                if(bird.row == i && col == 1)
                    pixels[i][col] = LOW;
                else
                    pixels[i][col] = HIGH;
            }
    }
    void destroy()
    {
        for(int i = 0; i < 8; i++)
        {
            if(bird.row == i && col == 1)
                pixels[i][col] = LOW;
            else
                pixels[i][col] = HIGH;
        }
    }
    void randomize()
    {
        start = random(0,6);
    }
    void move()
    {
        destroy();
        col--;
        if(col < 0)
        {
            col = 7;
            randomize();
        }
        create();

    }

};

byte n = 0;
Obstacle ob1;
Obstacle ob2;

void move()
{
    assigned = 0;
    ob1.move();
}
void move2()
{
    assigned = 0;
    ob2.move();
}
void create()
{
    n++;
    ob2.create();
    move2ID = t.every(obs_speed, move2);
}
void fall()
{
    bird.fall();
}

void setup()
{
    score = 0;
    n = 0;
    Serial.begin(9600);
    pinMode(inputPin, INPUT);
    bird.create();
    for(int i = 0; i < 8; i++)
    {
        pinMode(rows[i], OUTPUT);
        pinMode(cols[i], OUTPUT);

        digitalWrite(cols[i], HIGH);
    }

    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            pixels[i][j] = HIGH;
        }
    }
    ob1.create();
    fallID = t.every(gravity, fall);
    move1ID = t.every(obs_speed, move);
    t.after(2000, create);
}

void loop()
{
    refreshScreen();
    t.update();
    int reading = digitalRead(inputPin);
    if(reading != lastButtonState)
        lastDebounceTime = millis();
    if(millis() - lastDebounceTime > debounceDelay)
    {
        if(reading!=buttonState)
        {
            buttonState = reading;
            if(buttonState == HIGH)
            {
                t.stop(fallID);
                bird.jump();
                refreshScreen();
                fallID = t.every(gravity, fall);
            }
        }
    }
    lastButtonState = reading;
    collision();

}

void refreshScreen()
{
    for(int i = 0; i < 8; i++)
    {
        digitalWrite(rows[i], HIGH);
        for(int j = 0; j <8; j++)
        {
            int coord = pixels[i][j];
            if(coord == LOW)
            {
                digitalWrite(cols[j], coord);
            }
            digitalWrite(cols[j], HIGH);
        }

        digitalWrite(rows[i], LOW);
    }
}

void collision()
{
    if(ob1.col == 1 || ob2.col == 1)
    {
        Obstacle* a = &ob1;
        if(ob2.col == 1)
            a = &ob2;
        if(bird.row < a->start || bird.row >= a->start +3)
            error();
        else
        {
            if(assigned == 0)
            {
                score++;
                assigned = 1;
            }
        }
    }
}

void error()
{
    Serial.println(score);
    ob1.destroy();
    ob2.destroy();
    t.stop(fallID);
    t.stop(move1ID);
    t.stop(move2ID);
    ob1.col = 7;
    ob2.col =7;
    pixels[bird.row][1] = HIGH;
    for(int j = 0; j<2; j++)
    {
        int start = 0;
        int end = 7;
        for(int i = 0; i < 8; i++)
        {
            pixels[i][start] = LOW;
            pixels[i][end] = LOW;
            start++;
            end--;
        }
        int it = 0;
        while(it < 500)
        {
            refreshScreen();
            it++;
        }
        delay(100);
        start = 0;
        end = 7;
        for(int i = 0; i < 8; i++)
        {
            pixels[i][start] = HIGH;
            pixels[i][end] = HIGH;
            start++;
            end--;
        }
    }
    writeScore();
    setup();
}

void writeScore()
{
    int number[10][8][4] =
    {
        {
            {0,1,1,0},
            {1,0,0,1},
            {1,0,0,1},
            {1,0,0,1},
            {1,0,0,1},
            {1,0,0,1},
            {1,0,0,1},
            {0,1,1,0},
        },
        {
            {0,1,1,0},
            {1,0,1,0},
            {0,0,1,0},
            {0,0,1,0},
            {0,0,1,0},
            {0,0,1,0},
            {0,0,1,0},
            {1,1,1,1},
        },
        {
            {0,1,1,0},
            {1,0,0,1},
            {0,0,0,1},
            {0,0,1,0},
            {0,1,0,0},
            {1,0,0,0},
            {1,0,0,0},
            {1,1,1,1},
        },
        {
            {0,1,1,0},
            {1,0,0,1},
            {0,0,0,1},
            {0,1,1,0},
            {0,0,0,1},
            {1,0,0,1},
            {0,1,1,0},
            {0,0,0,0},
        },
        {
            {1,0,0,1},
            {1,0,0,1},
            {1,0,0,1},
            {1,1,1,1},
            {0,0,0,1},
            {0,0,0,1},
            {0,0,0,1},
            {0,0,0,1},
        },
        {
            {1,1,1,1},
            {1,0,0,0},
            {1,0,0,0},
            {1,1,1,0},
            {0,0,0,1},
            {0,0,0,1},
            {1,0,0,1},
            {0,1,1,0},
        },
        {
            {0,1,1,0},
            {1,0,0,0},
            {1,0,0,0},
            {1,1,1,0},
            {1,0,0,1},
            {1,0,0,1},
            {1,0,0,1},
            {1,1,1,0},
        },
        {
            {1,1,1,1},
            {0,0,0,1},
            {0,0,0,1},
            {0,0,0,1},
            {0,0,0,1},
            {0,0,0,1},
            {0,0,0,1},
            {0,0,0,1},
        },
        {
            {0,1,1,0},
            {1,0,0,1},
            {1,0,0,1},
            {0,1,1,0},
            {1,0,0,1},
            {1,0,0,1},
            {1,0,0,1},
            {0,1,1,0},
        },
        {
            {0,1,1,0},
            {1,0,0,1},
            {1,0,0,1},
            {1,0,0,1},
            {0,1,1,1},
            {0,0,0,1},
            {1,0,0,1},
            {0,1,1,0},
        }
    };
    int a = score /10;
    int b = score %b;

    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            if(number[a][i][j])
                pixels[i][j] = LOW;
            else
                pixels[i][j] = HIGH;

            if(number[b][i][j])
                pixels[i][j+4] = LOW;
            else
                pixels[i][j+4] = HIGH;
        }
        Serial.println();
    }
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {

            Serial.print(pixels[i][j]);
        }
        Serial.println();
    }

    int it = 0;
    while(it < 1000)
    {
        refreshScreen();
        it++    ;
    }

}
